import { addTask } from "./tasksApi";

export let addTestTask = () => {
    let testTask = {};
    testTask.id = 0;
    testTask.title = 'test task 999';
    testTask.description = 'description for test task 999';
    testTask.startDate = '2018-12-25';
    testTask.dueDate = '2018-12-25';
    testTask.hours = "10";
    addTask(testTask).then(data => {
        return data;
    });
}

