import axios from 'axios';
import { baseUrl } from '../config';

export let getAllTasks = () => {
    return axios
        .get('/tasks', { baseURL: baseUrl })
        .then(response => {
            return response.data;
        })
        .catch(err => { console.log(err) })
}

export let getTaskById = (taskId) => {
    return axios
        .get(`/task/${taskId}`, { baseURL: 'http://localhost:8080' })
        .then(response => { 
            return response.data;
        })
        .catch(err => { console.log(err) })
}

export let addTask = (taskObject) => {
    return axios
        .post(`/task`, taskObject, { baseURL: 'http://localhost:8080' })
        .then(response => { 
            return response.data;
        })
        .catch(err => { console.log(err) })
}

export let updateTask = (taskObject) => {
    return axios
        .put(`/task`, taskObject, { baseURL: 'http://localhost:8080' })
        .then(response => { 
            return response.data;
        })
        .catch(err => { console.log(err) })
}

export let deleteTask = (taskId) => {
    return axios
        .delete(`/tasks/${taskId}`, { baseURL: 'http://localhost:8080' })
        .then(response => { 
            return response.data;
        })
        .catch(err => { console.log(err) })
}