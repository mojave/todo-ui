import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { addTestTask } from "./API/addTestTask"

class SideNav extends Component {
  state = {
    tasksDropdown: { classes: 'dropdown-btn' }
  }

  handleClick = (e) => {
    e.target.classList.toggle("active");
    var dropdownContent = this.getNextSibling(e);
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  }

  /* workaround to handle when someone clicks *
   * on the fa-caret-down, which doesn't have *
   * a nextElementSibling */
  getNextSibling = (e) => {
    var el = e.target.nextElementSibling;
    if (el) return el;
    var elParent = e.target.parentElement;
    el = elParent.nextElementSibling;
    return el;
  }

  render() {
    return (
      <div className="sidenav">
        <Link to="/home">home</Link>
        <button id='tasksDropdown' className={this.state.tasksDropdown.classes} onClick={this.handleClick}>
          tasks
            <i className="fa fa-caret-down"></i>
        </button>
        <div className="dropdown-container">
          <Link to="/allTasks" >all tasks</Link>
          <Link to="/addTask" >add task</Link>
          <a onClick={() => addTestTask()} href="/allTasks">add test task</a>
        </div>
      </div>
    );
  }
}

export default SideNav;
