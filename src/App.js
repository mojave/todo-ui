import React, { Component } from 'react';
import {BrowserRouter} from 'react-router-dom';
import SideNav from './SideNav';
import Main from './Main';
import './tachyons.min.css';
import './App.css';

class App extends Component {

  render() {
    return (
      // sidenav and main css work together
      <BrowserRouter>
        <section className="mw5 mw7-ns center pa3 ph5-ns">
          <SideNav />
          <Main />
        </section>
      </BrowserRouter>
    );
  }
}

export default App;
