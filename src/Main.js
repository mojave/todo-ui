import React, { Component } from 'react';
import TasksViewer from './components/Form/TasksViewer';
import AddTask from './components/Form/AddTask';
import Task from './components/Task/Task';
import { Route } from 'react-router-dom';
import Home from './components/Home';

class Main extends Component {
  render() {
    return (
      <div>
        <Route exact path="/allTasks" component={TasksViewer} />
        <Route exact path="/addTask" component={AddTask} />
        <Route exact path="/task/:id" component={Task} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/" component={Home} />
      </div>
    );
  }
}

export default Main;
