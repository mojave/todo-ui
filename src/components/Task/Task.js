import React from 'react';
import { getTaskById, updateTask } from '../../API/tasksApi';


class Task extends React.Component {
    state = {
        task: { id: '', title: '', description: '', hours:'', startDate: '', dueDate:'' }
    }

    componentDidMount() {
        const pathParts = this.props.location.pathname.split('/');
        console.log(`pathParts: ${JSON.stringify(pathParts)}`)
        let taskId = pathParts[pathParts.length - 1];
        getTaskById(taskId)
            .then(data => {
                console.log(`data: ${JSON.stringify(data)}`)
                this.setState({ task: data })
            })
    }

    handleChange = event => {
        let task = {...this.state.task}
        task[event.target.id] = event.target.value;
        this.setState({ task: task });
    };

    updateTask = () => {
        updateTask(this.state.task).then(data => {
           this.setState({task: data})
        });
    }

    render() {
        // console.log(`this.state.task: ${JSON.stringify(this.state.task)}`)
        return (
            <div style={{ paddingTop: '20px', border: '1px solid #aaa' }}>
                <form>
                    task id:
              <br />
              {this.state.task.id}
                    {/* <input
                        className="ba b--silver mb1 addTaskInput"
                        id="id"
                        type="text"
                        placeholder='enter task id'
                        value={this.state.task.id ? this.state.task.id : 'enter task id'}
                        onChange={this.handleChange}
                    /> */}
                    <br />
                    task title:
              <br />
                    <input
                        className="ba b--silver mb1 addTaskInput"
                        id="title"
                        type="text"
                        placeholder='enter title'
                        value={this.state.task.title}
                        onChange={this.handleChange}
                    />
                    <br />
                    task description:
              <br />
                    <input
                        className="ba b--gold mb1 addTaskInput"
                        id="description"
                        type="text"
                        placeholder='enter description'
                        value={this.state.task.description}
                        onChange={this.handleChange}
                    />
                    <br />
                    hours:
              <br />
                    <input
                        className="ba b--blue mb1 addTaskInput"
                        id="hours"
                        type="text"
                        placeholder='enter hours'
                        value={this.state.task.hours}
                        onChange={this.handleChange}
                    />
                    <br />
                    start date:
              <br />
                    <input
                        className="ba b--orange mb1 addTaskInput"
                        id="startDate"
                        type="date"
                        value={this.state.task.startDate}
                        onChange={this.handleChange}
                    />
                    <br />
                    end date:
              <br />
                    <input
                        className="ba b--blue mb1 addTaskInput"
                        id="dueDate"
                        type="date"
                        value={this.state.task.dueDate ? this.state.task.dueDate : ''}
                        onChange={this.handleChange}
                    />
                    <br />
                    <br />
                </form>
                <div className="ph3 mt2">
                    <a
                        className="f6 link dim br1 ba ph3 pv1 mb1 dib white bg-mid-gray"
                        href="/allTasks"
                        onClick={this.updateTask}
                    >
                        Submit
              </a>
                </div>
            </div>
        )
    }
}

export default Task;