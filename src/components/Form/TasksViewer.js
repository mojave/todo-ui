import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { deleteTask, getAllTasks, getTaskById } from "../../API/tasksApi";
import TableWithSelect from '../Table/TableWithSelect';

/* 
 * TODO: 
 * * make components of all the subsections
 */

// https://reactjs.org/docs/forms.html
class TasksViewer extends Component {
    state = {
        selectedRows: [],
        mode: "",
        readTaskId: '',
        newTaskId: "",
        newTaskTitle: "",
        newTaskDescription: "",
        newTaskHours: "",
        newTaskStartDate: "",
        newTaskEndDate: ""
    };

    componentDidMount(){
        this.retrieveAllTasks();
    }

    handleChange = event => {
        this.setState({ [event.target.id]: event.target.value, mode:'' });
    };

    retrieveAllTasks = () => {
        getAllTasks().then(data => {
            this.setState({ tasks: data });
        });
    }

    removeTaskFromState(id) {
        let currTasks = [...this.state.tasks];
        this.state.tasks.forEach((task, index) => {
            if (task.id === id) {
                currTasks.splice(index, 1);
            }
        })
        this.setState({ tasks: currTasks });
    }

    editTask = () => {
        let selectedRows = this.state.selectedRows;
        if (!selectedRows.length > 0) {
            // showErrorModal
        }else{
            let idOfTaskToEdit = selectedRows[selectedRows.length-1];
            // getTaskById(idOfTaskToEdit)
            // .then( data => {

            // })
        }
    }

    deleteTasks = () => {
        let updatedSelectedRows = [...this.state.selectedRows];
        this.state.selectedRows.forEach(
            (el, index) => {
                deleteTask(el)
                    .then(response => {
                        updatedSelectedRows.splice(index, 1);
                        this.removeTaskFromState(el);
                    })
            })
        this.setState({ selectedRows: updatedSelectedRows });
    };

    onRowSelect = (event) => {
        let rowId = event.target.id;
        let updatedSelectedRows = [...this.state.selectedRows];
        if (this.rowAlreadySelected(rowId)) {
            let indexOfRow = updatedSelectedRows.indexOf(rowId);
            updatedSelectedRows.splice(indexOfRow, 1);
        } else {
            updatedSelectedRows.push(rowId);
        }
        // console.log(`selectedRows: ${JSON.stringify(updatedSelectedRows)}`)
        this.setState({ 
            selectedRows: updatedSelectedRows,
            lastSelectedRow: rowId
        })
    }

    rowAlreadySelected(id) {
        let isSelected = false;
        if (this.state.selectedRows.indexOf(id) !== -1) {
            isSelected = true;
        }
        return isSelected;
    }


    render() {
        const headers = ['id', 'title', 'description', 'start', 'due', 'hours'];
        return (
            <div>
                {/* if we have tasks, display them. */}
                {this.state.tasks
                    ?
                    <div>
                        <TableWithSelect headers={headers} data={this.state.tasks} onRowSelect={this.onRowSelect} />
                        <div className="ph3 mt3">
                            <a className="f6 link dim br1 ba ph3 pv1 mb1 dib white bg-mid-gray" href='/home'>Done</a>
                            <Link className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib light-yellow bg-mid-gray" to={`/task/${this.state.lastSelectedRow}`}>Edit</Link>
                            <a className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib light-yellow bg-mid-gray" onClick={this.deleteTasks} >Delete</a>
                        </div>
                    </div>
                    :
                    null}
            </div>
        );
    }
}

export default TasksViewer;
