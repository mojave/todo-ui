import React, { Component } from "react";
import { addTask } from "../../API/tasksApi";
import { get } from 'lodash';

// https://reactjs.org/docs/forms.html
class AddTask extends Component {
  state = {}

  handleChange = event => {
    // console.log(`handleChange: ${event.target.id}, ${event.target.value}`)
    this.setState({ [event.target.id]: event.target.value });
  };

  addTask = () => {
    let newTask = this.createNewTaskObject();
    addTask(newTask).then(data => {
      this.setState({ tasks: [data] });
    });
  }

  createNewTaskObject = () => {
    let newTask = {};
    newTask.id = get(this.state, 'newTaskId', 0);
    newTask.title = get(this.state, 'newTaskTitle', '');
    newTask.description = get(this.state, 'newTaskDescription', '');
    newTask.hours = get(this.state, 'newTaskHours', "0");
    newTask.startDate = get(this.state, 'newTaskStartDate', '2019-12-31');
    newTask.dueDate = get(this.state, 'newTaskEndDate', '2019-12-31');
    return newTask;
  }

  render() {
    return (
      <div style={{paddingTop:'20px', border:'1px solid #aaa'}}>
        <form>
          task title:
          <br />
          <input
            className="ba b--silver mb1 addTaskInput"
            id="newTaskTitle"
            type="text"
            placeholder='enter title'
            onChange={this.handleChange}
          />
          <br />
          task description:
          <br />
          <input
            className="ba b--gold mb1 addTaskInput"
            id="newTaskDescription"
            type="text"
            placeholder='enter description'
            onChange={this.handleChange}
          />
          <br />
          hours:
          <br />
          <input
            className="ba b--blue mb1 addTaskInput"
            id="newTaskHours"
            type="text"
            placeholder='enter hours'
            onChange={this.handleChange}
          />
          <br />
          start date:
          <br />
          <input
            className="ba b--orange mb1 addTaskInput"
            id="newTaskStartDate"
            type="date"
            onChange={this.handleChange}
          />
          <br />
          end date:
          <br />
          <input
            className="ba b--blue mb1 addTaskInput"
            id="newTaskEndDate"
            type="date"
            onChange={this.handleChange}
          />
          <br />
          <br />
        </form>
        <div className="ph3 mt2">
          <a
            className="f6 link dim br1 ba ph3 pv1 mb1 dib white bg-mid-gray"
            href="/allTasks"
            onClick={this.addTask}
          >
            Submit
          </a>
        </div>
      </div>
    );
  }
}

export default AddTask;
