import React, { Component } from "react";
import { addTask, deleteTask, getAllTasks, getTaskById } from "../../API/tasksApi";
// import Table from '../Table/Table.js';
import TableWithSelect from '../Table/TableWithSelect';
import AddTask from './AddTask'
import { get } from 'lodash';

/* 
 * TODO: 
 * * make components of all the subsections
 */

// https://reactjs.org/docs/forms.html
class Form extends Component {
    state = {
        selectedRows: [],
        mode: "",
        readTaskId: '',
        newTaskId: "",
        newTaskTitle: "",
        newTaskDescription: "",
        newTaskHours: "",
        newTaskStartDate: "",
        newTaskEndDate: ""
    };

    handleAddTestTask = this.handleAddTestTask.bind(this);

    handleChange = event => {
        // console.log(`handleChange: ${event.target.id}, ${event.target.value}`)
        this.setState({ [event.target.id]: event.target.value, mode:'' });
    };

    // retrieveAllTasks = () => {
    //     getAllTasks().then(data => {
    //         this.setState({ tasks: data });
    //     });
    // }

    retrieveTaskById = () => {
        getTaskById(this.state.readTaskId).then(data => {
            this.setState({ tasks: [data] });
        });
    }

    handleGetTaskById = () => {
        this.setState({ mode: 'taskById', tasks: null })
    }

    handleAddTask = () => {
        this.setState({ mode: 'addTask', tasks: null })
    }

    addTask = () => {
        let newTask = this.createNewTaskObject();
        addTask(newTask).then(data => {
            this.setState({ tasks: [data] });
        });
    }

    createNewTaskObject = () => {
        let newTask = {};
        newTask.id = get(this.state, 'newTaskId', 0);
        newTask.title = get(this.state, 'newTaskTitle', '');
        newTask.description = get(this.state, 'newTaskDescription', '');
        newTask.hours = get(this.state, 'newTaskHours', "0");
        newTask.startDate = get(this.state, 'newTaskStartDate', '12/25/2018');
        newTask.endDate = get(this.state, 'newTaskEndDate', '12/25/2018');
        return newTask;
    }

    removeTaskFromState(id) {
        let currTasks = [...this.state.tasks];
        this.state.tasks.forEach((task, index) => {
            if (task.id === id) {
                currTasks.splice(index, 1);
            }
        })
        this.setState({ tasks: currTasks });
    }

    deleteTasks = () => {
        let updatedSelectedRows = [...this.state.selectedRows];
        this.state.selectedRows.forEach(
            (el, index) => {
                deleteTask(el)
                    .then(response => {
                        updatedSelectedRows.splice(index, 1);
                        this.removeTaskFromState(el);
                    })
            })
        this.setState({ selectedRows: updatedSelectedRows });
    };

    done = () => {
        this.setState({ mode: "", tasks: null });
    };

    onRowSelect = (event) => {
        let rowId = event.target.id;
        let updatedSelectedRows = [...this.state.selectedRows];
        if (this.rowAlreadySelected(rowId)) {
            let indexOfRow = updatedSelectedRows.indexOf(rowId);
            updatedSelectedRows.splice(indexOfRow, 1);
        } else {
            updatedSelectedRows.push(rowId);
        }
        this.setState({ selectedRows: updatedSelectedRows })
    }

    rowAlreadySelected(id) {
        let isSelected = false;
        if (this.state.selectedRows.indexOf(id) !== -1) {
            isSelected = true;
        }
        return isSelected;
    }

    handleAddTestTask() {
        let testTask = {};
        testTask.id = 0;
        testTask.title = 'test task 999';
        testTask.description = 'description for test task 999';
        testTask.startDate = '12-25-2018';
        testTask.endDate = '12-25-2018';
        testTask.hours = "10";
        addTask(testTask).then(data => {
            this.setState({ tasks: [data] });
        });
    }

    render() {
        const headers = ['id', 'title', 'description', 'start', 'due', 'hours'];
        return (
            <div>
                <div className="ph3 mt2">
                    <a className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib white bg-mid-gray" onClick={this.retrieveAllTasks} href='/'>get-all-tasks</a>
                    <a className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib white bg-mid-gray" onClick={this.handleGetTaskById} href='/'>task-by-id</a>
                    <a className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib white bg-mid-gray" onClick={this.handleAddTask} href='/'>add-task</a>
                    <a className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib white bg-mid-gray" onClick={this.handleAddTestTask} href='/'>add-test-task</a>
                </div>

                {/* if we have tasks, display them. */}
                {this.state.tasks
                    ?
                    <div>
                        <TableWithSelect headers={headers} data={this.state.tasks} onRowSelect={this.onRowSelect} />
                        <div className="ph3 mt3">
                            <a className="f6 link dim br1 ba ph3 pv1 mb1 dib white bg-mid-gray" onClick={this.done} href='/'>Done</a>
                            <a className="f6 link dim br1 ba ph3 pv1 mb1 ml1 dib light-yellow bg-mid-gray" onClick={this.deleteTasks} href='/'>Delete</a>
                        </div>
                    </div>
                    :
                    null}

                {/* if they clicked 'read' (get single task by id), show the appropriate form */}
                {this.state.mode === "taskById"
                    ?
                    <div>
                        <form>
                            task id:
                        <br />
                            <input
                                id="readTaskId"
                                type="text"
                                value={this.state.readTaskId}
                                onChange={this.handleChange}
                            />
                        </form>
                        <div className="ph3 mt2">
                        {/* // eslint-disable-next-line */}
                            <a className="f6 link dim br1 ba ph3 pv1 mb1 dib white bg-mid-gray" onClick={this.retrieveTaskById} href='/'>Submit</a>
                        </div>
                    </div>
                    :
                    null}

                {/* if they clicked 'add', show the appropriate form */}
                {this.state.mode === "addTask"
                    ?
                    <AddTask handleChange={this.handleChange} addTask={this.addTask} {...this.props} />
                    :
                    null}

            </div>
        );
    }
}

export default Form;
