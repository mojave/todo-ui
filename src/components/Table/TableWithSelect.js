import React from "react";
import { get } from "lodash";
import "./table.css";

/** Generic Table which takes an ato brray of headers and an array of rowData.
 * Eliminates need for including bootstrap, (e.g. when using react-bootstrap-table)
 *
 * props:
 *   * onRowSelect callback
 *
 * TODO:
 *   * create/update styling to be self-contained and non-dependent upon tachyons.
 *   * allow supplying of custom css
 *   * add color highlighting of selected rows
 *   * add ability to provide only rowData, and the component will generate the list of headers from the rowData.
 *   * need to use an actual 'id' attribute, instead of an index in the array.
 *
 * */
const Table = props => {
  const validate = props => {
    let headerCount = props.headers.length;
    props.data.forEach(row => {
      if (Object.keys(row).length !== headerCount) return false;
    });
    return true;
  };

  const convertRowDataToArray = rowData => {
    let newRowData = [];
    rowData.forEach(obj => {
      let arr = Object.values(obj).map(value => {
        return value;
      });
      newRowData.push(arr);
    });
    return newRowData;
  };

  const onRowSelect = e => {
    props.onRowSelect(e);
  };

  const generateRowHeaders = () => {
    let headers = get(props, "headers", []);
    return headers;
  };

  return validate(props) ? (
    <table className="collapse">
      <tbody>
        <tr id="header-row" className="stripe-mid-dark">
          <th className="tc pa2">Select</th>
          {generateRowHeaders().map(header => {
            return (
              <th key={header} className="tc pa2">
                {header}
              </th>
            );
          })}
        </tr>
        {convertRowDataToArray(props.data).map((row, index) => {
          return (
            <tr key={row[0]} className="stripe-mid-dark">
              <td className="tc pa2">
                <input
                  id={row[0]}
                  type="checkbox"
                  name="select"
                  onClick={onRowSelect}
                />
              </td>
              {row.map((cell, index) => {
                return (
                  <td key={index} className="tc pa2">
                    {cell}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  ) : (
    <p>invalid table data</p>
  );
};

export default Table;
