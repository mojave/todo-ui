import React from "react";
import "./table.css";

/** Generic Table which takes an array of headers and an array of rowData.
 * - eliminates need for including bootstrap. */
const Table = props => {
    //TODO: Add ability to provide only rowData, and the component will 
    //generate the list of headers from the rowData.
    const validate = (props) => {
        let headerCount = props.headers.length;
        props.data.forEach(row => {
            if (Object.keys(row).length !== headerCount) return false;
        })
        return true;
    }
    validate(props);

    const convertRowDataToArray = rowData => {
        let newRowData = [];
        rowData.forEach(obj => {
            let arr = Object.values(obj).map(value => {
                return value
            });
            newRowData.push(arr)
        });
        return newRowData;
    };

    const headers = props.headers;
    const rowData = convertRowDataToArray(props.data);

    return (
        <table>
            <tbody>
                <tr id="header-row">
                    {headers.map(header => {
                        return <th key={header}>{header}</th>;
                    })}
                </tr>
                {rowData.map( (row,index) => {
                    return (
                        <tr key={index}>
                            {row.map( (cell,index) => {
                                return <td key={index}>{cell}</td>
                            })}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
};

export default Table;
